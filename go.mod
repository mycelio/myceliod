module myceliod

go 1.17

require (
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
	gitlab.com/mycelio/cmdutil/config v0.0.0-20210808154541-3f8636e8d3b3
	gitlab.com/mycelio/cmdutil/logger v0.0.0-20210808154541-3f8636e8d3b3
	gitlab.com/mycelio/myceliod/mycelio v0.0.0-00010101000000-000000000000
	google.golang.org/grpc v1.39.1
)

require (
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-kit/kit v0.11.0 // indirect
	github.com/go-logfmt/logfmt v0.5.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/magiconair/properties v1.8.5 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/pelletier/go-toml v1.9.3 // indirect
	github.com/spf13/afero v1.6.0 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/subosito/gotenv v1.2.0 // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sys v0.0.0-20210603081109-ebe580a85c40 // indirect
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/genproto v0.0.0-20210602131652-f16073e35f0c // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

replace gitlab.com/mycelio/myceliod/mycelio => ./mycelio
