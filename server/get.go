package server

import (
	"context"
	"fmt"
	"gitlab.com/mycelio/cmdutil/logger"
	pb "gitlab.com/mycelio/myceliod/mycelio"
)

//goland:noinspection GoErrorStringFormat
func (s *server) Get(ctx context.Context, in *pb.GetRequest) (*pb.GetResponse, error) {
	logger.Debug("event", "Call", "method", "Get", "resource", in.GetResource(), "name", in.GetName())
	return nil, fmt.Errorf("Unknown resource type \"%s\"", in.GetResource())
}
