package server

import (
	"context"
	"gitlab.com/mycelio/cmdutil/logger"
	pb "gitlab.com/mycelio/myceliod/mycelio"
)

func (s *server) GetVersion(ctx context.Context, in *pb.GetVersionRequest) (*pb.GetVersionResponse, error) {
	logger.Debug("event", "Call", "method", "GetVersion")
	return &pb.GetVersionResponse{Version: &pb.Version{Major: "0", Minor: "0", Patch: "1"}}, nil
}
