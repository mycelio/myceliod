package server

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/mycelio/cmdutil/logger"
	pb "gitlab.com/mycelio/myceliod/mycelio"
	"google.golang.org/grpc"
	"net"
	"os"
	"os/signal"
	"syscall"
)

type server struct {
	pb.UnimplementedMycelioServer
	config Config
}

type Config struct {
	Address string
}

func NewServer(config Config) *server {
	s := server{
		config: config,
	}

	return &s
}


func (s *server) Start() error {
	initInterruptionHandler()

	listener, err := net.Listen("tcp", s.config.Address)
	cobra.CheckErr(err)

	grpcServer := grpc.NewServer()
	pb.RegisterMycelioServer(grpcServer, s)

	logger.Info("msg", "Server started", "addr", listener.Addr())
	return grpcServer.Serve(listener)
}

func initInterruptionHandler()  {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		cleanup()

		os.Exit(0)
	}()

	fmt.Println("\nPress CTRL+C to interrupt")
}

func cleanup() {

}
