package server

import (
	"context"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"myceliod/server"
)

type Options struct {
}

func NewCmd(options Options) *cobra.Command {

	cmd := &cobra.Command{
		Use:   "server",
		Short: "Start server",
		Run: func(cmd *cobra.Command, args []string) {
			options.Run(cmd.Context(), cmd, args)
		},
	}

	cmd.PersistentFlags().StringP("address", "a", ":5000", "Server network address")

	viper.BindPFlag("server.address", cmd.PersistentFlags().Lookup("address"))

	return cmd
}

func (o *Options) Run(ctx context.Context, cmd *cobra.Command, args []string) {
	s := server.NewServer(server.Config{
		Address: viper.GetString("server.address"),
	})

	err := s.Start()
	cobra.CheckErr(err)
}
