package cmd

import (
	"context"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mycelio/cmdutil/config"
	"gitlab.com/mycelio/cmdutil/logger"
	"myceliod/cmd/server"
)

type Options struct {
}


func NewCmd(options Options) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "myceliod",
		Short: "Mycelio daemon",
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			initConfig()
			initLogger()

			logger.Debug("config", viper.ConfigFileUsed())
		},
	}

	// Flags
	cmd.PersistentFlags().String("config", "", "Config file")
	cmd.PersistentFlags().CountP("verbose", "v", "Verbosity level")

	viper.BindPFlag("logging.verbosity", cmd.PersistentFlags().Lookup("verbose"))
	viper.BindPFlag("config", cmd.PersistentFlags().Lookup("config"))

	// Commands
	cmd.AddCommand(server.NewCmd(server.Options{}))

	return cmd
}

func (o *Options) Run(ctx context.Context, cmd *cobra.Command, args []string) {

}

func Execute() {
	rootCmd := NewCmd(Options{})

	err := rootCmd.Execute()
	cobra.CheckErr(err)
}

func initLogger() {
	logger.NewLogger(logger.Options{
		Verbosity: viper.GetInt("logging.verbosity"),
	})
}

func initConfig() {
	config.Configure(config.Options{
		Path: viper.GetString("config"),
		DefaultFileName: ".myceliod",
	})
}
